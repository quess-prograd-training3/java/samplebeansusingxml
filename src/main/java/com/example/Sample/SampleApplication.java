package com.example.Sample;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SpringBootApplication
public class SampleApplication {

	public static void main(String[] args) {

		//SpringApplication.run(SampleApplication.class, args);
		ApplicationContext context=new ClassPathXmlApplicationContext("XmlFile.xml");
		BeanUsingXml beanUsingXmlObject=context.getBean("PersonDetails", BeanUsingXml.class);
		beanUsingXmlObject.display();
	}

}
